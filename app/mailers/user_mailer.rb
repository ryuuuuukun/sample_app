class UserMailer < ApplicationMailer
default from: "pleplesample@gmail.com"
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.account_activation.subject
  #
  def account_activation(user)
    @user = user
    mail to: user.email, subject: "Account activation"
end
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user) #関数のように受け取った値をuserに格納
    @user = user #インスタンス変数にuserには行った値を代入
    p user
    mail to: user.email, subject: "Password reset" #インスタンス変数内のmailを取得
  end

  
end


