class ApplicationMailer < ActionMailer::Base
  default from: "pleplesample@gmail.com"
  layout 'mailer'
end
