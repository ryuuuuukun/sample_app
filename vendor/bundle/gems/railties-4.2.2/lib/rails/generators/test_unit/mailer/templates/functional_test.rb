require 'test_helper'

<% module_namespacing do -%>
class <%= class_name %>Test < ActionMailer::TestCase
<% actions.each do |action| -%>
  test "<%= action %>" do
    mail = <%= class_name %>.<%= action %>
    assert_equal <%= action.to_s.humanize.inspect %>, mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["pleplesample@gamil.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

<% end -%>
<% if actions.blank? -%>
  # test "the truth" do
  #   assert true
  # end
<% end -%>
end
<% end -%>
